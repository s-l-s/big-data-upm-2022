ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.12.15"

organization := "upm.bd"
name := "sparkapp"
version := "1.0.0"

libraryDependencies += "org.apache.spark" %% "spark-core" % "3.3.0"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "3.3.0"
libraryDependencies += "org.apache.spark" %% "spark-mllib" % "3.3.0"

lazy val root = (project in file("."))
  .settings(
    name := "finalVersion"
  )
