package upm.bd
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions._
import org.apache.spark
import org.apache.spark.ml.feature.{StringIndexer, VectorAssembler}
import org.apache.spark.ml.stat.Correlation
import org.apache.spark.mllib.linalg.Matrix
import org.apache.spark.sql.{DataFrame, Dataset, Encoders, Row, SQLContext, SparkSession}
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import upm.bd.Processing.createNewIndexList

import upm.bd.Model
import upm.bd.LinearModel
import upm.bd.customRandomForestRegressionModel
import upm.bd.customDecisionTreeRegressionModel

object MyApp {

  /** Hadoop configuration */
  /*val conf = new SparkConf().setAppName("My first Spark application")
  val spark = SparkSession.builder().master("local").getOrCreate()
  val sc = new SparkContext(new SparkConf().setAppName())
  val sqlContext = new SQLContext(sc);
  */
  /** Local configuration * */
  val spark = SparkSession
    .builder()
    .appName("MyApp")
    .master(master = "local[16]")
    .config("spark.driver.memory", "16G")
    .getOrCreate();
  spark.sparkContext.setLogLevel("ERROR")


  val sqlContext = new SQLContext(spark.sparkContext)
  import spark.implicits._

  case class index_list(UniqueCarrier: String, UniqueCarrierIndex: Int, Dest: String, DestIndex: Int, Origin: String, OriginIndex: Int, TailNum: String, TailNumIndex: Int, Type: String, typeIndex: Int, manufacturer: String, manufacturerIndex: Int, model: String, modelIndex: Int, status: String, statusIndex: Int, aircraft_type: String, aircraft_typeIndex: Int, engine_type: String, engine_typeIndex: Int)

  var indexlist = Seq.empty[index_list].toDF()
  def main(args : Array[String]): Unit = {

    /**Final main method should look like this*/
    println("UPM Big-Data-Assignment 22/23")
    print("Make sure that the yearly file xxxx.csv, airport.csv and plane-data.csv are uploaded to hdfs, using hdfs dfs -put filename")
    val data_path = "hdfs://localhost:9000/data/"
    /**We are going to use the following var's containing local paths as strings to read the files from hdfs*/
    /*var inputFileYear: String = ""
    var inputAirport: String = ""
    var inputPlaneData: String = ""

    while(inputFileYear.equals("") && inputAirport.equals("") && inputPlaneData.equals("")){
      inputFileYear = scala.io.StdIn.readLine("Please provide your hdfs path for the year file: ")
      inputAirport = scala.io.StdIn.readLine("Please provide your hdfs path for the airport.csv file: ")
      inputPlaneData = scala.io.StdIn.readLine("Please provide your hdfs path for the plane-data.csv file: ")
    }*/

    var dfPlane = sqlContext.read.format("csv")
      .option("header", "true")
      .option("delimiter", ",")
      .option("inferSchema", "true").load(data_path + "plane-data.csv")

    var df1995 = sqlContext.read.format("csv")
      .option("header", "true")
      .option("delimiter", ",")
      .option("inferSchema", "true").load(data_path + "1995.csv")

    /** Do we actually need airports file?
    val dfAirports = sqlContext.read.format("csv")
      .option("header", "true")
      .option("delimiter", ",")
      .option("inferSchema", "true").load(data_path + "airports.csv")
    */
    println("Start of the preprocessing & cleaning of the data")
    var dfMerged= Processing.controlCleaning(df1995, dfPlane)
    println("Preprocessing & cleaning is finished")
    
    println("Start of the model creating part")
    var selectedML: Int = 0
    while (selectedML != 1 && selectedML != 2 && selectedML != 3) {
      println("Which machine learning technique do you want to use?")
      println("[1] Linear Regression")
      println("[2] Decision Tree Regression")
      println("[3] Random Forest Regression")
      selectedML = scala.io.StdIn.readInt()
    }
    // TODO: Make this part less redundant :(
    if (selectedML == 1){
      println("Linear Regression Model")
      val lmModel = new LinearModel()
      val transformedDF = lmModel.transformInputData(dfMerged)
      val Array(trainingDF, testDF) = transformedDF.randomSplit(Array(0.7,0.3))
      val lmPredictions = lmModel.cvModel(trainingDF, testDF)
      lmPredictions.show(5)
      lmModel.printEvaluationMetrics(lmPredictions) 
      } else if (selectedML == 2){
      println("Decision Tree Regression Model")
      val dtModel = new customDecisionTreeRegressionModel()
      val dtTransformedDF = dtModel.transformInputData(dfMerged)
      val Array(dtTrainingDF, dtTestDF) = dtTransformedDF.randomSplit(Array(0.7,0.3))
      val dtPredictions = dtModel.cvModel(dtTrainingDF, dtTestDF)
      dtPredictions.show(5)
      dtModel.printEvaluationMetrics(dtPredictions) 
    } else if (selectedML == 3){
      println("Random Forest Regression Model")
      val rfrModel = new customRandomForestRegressionModel()
      val rfrTransformedDF = rfrModel.transformInputData(dfMerged)
      val Array(rfrTrainingDF, rfrTestDF) = rfrTransformedDF.randomSplit(Array(0.7,0.3))
      val rfrPredictions = rfrModel.cvModel(rfrTrainingDF, rfrTestDF)
      rfrPredictions.show(5)
      rfrModel.printEvaluationMetrics(rfrPredictions) 
}
    
    println("Start of the analysis")
    // TODO: Check, if path is valid
    println("The output of the analysis gets stored in a csv-file. We use them in order to create the plots in Excel.")
    var pathCsv = scala.io.StdIn.readLine("Please provide the path, where you would like to have the analysis data stored: ")
    Analysis.countAirports(dfMerged, indexlist, pathCsv)
    Analysis.getMonth(dfMerged, pathCsv)
    Analysis.getDay(dfMerged, pathCsv)
    Analysis.getHour(dfMerged, pathCsv)
    Analysis.summaryStatistics(dfMerged, pathCsv)
    Analysis.correlationMatrix(dfMerged)
    println("Analysis is finished")

  }

}
