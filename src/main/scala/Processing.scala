package upm.bd
import org.apache.spark
import org.apache.spark.ml.feature.StringIndexer
import org.apache.spark.sql.Row.empty.schema
import org.apache.spark.sql.{DataFrame, Dataset, Row}
import org.apache.spark.sql.functions.{col, concat, date_format, datediff, lit, regexp_replace, to_date}
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}

object Processing {
  case class clc_1995(year:String, month:Int, dayOfMonth: Int, dayOfWeek: Int, depTime: String, crsDepTime: Int, crsArrTime: Int, uniqueCarrier: String, flightNum: String, tailNum: String, crsElapsedTime: String, ArrDelay: String, depDelay: String, origin: String, dest: String, distance: String, TaxiOut: Int, cancelled: Int)

  var controlCleaning= (data: DataFrame, dataPlane: DataFrame)=>{

    var currentDataset = data
    val df1995originalcount = data.count()
    println(s"Original dataframe's count: $df1995originalcount")

    currentDataset = initialRemovement(currentDataset)
    currentDataset = checkEmpty(currentDataset)
    currentDataset = checkColumnValueDistribution(currentDataset)
    currentDataset = mergeDateColumns(currentDataset)
    currentDataset = castDataType(currentDataset)
    val df1995count = currentDataset.count()
    println(s"Original dataframe's count after data cleaning: $df1995count")

    currentDataset = mergeDataSets(currentDataset, dataPlane)
    val dfMergedCount = currentDataset.count()
    println(s"Merged dataframe's count: $dfMergedCount")
    currentDataset.show()

    currentDataset = calculateAge(currentDataset)
    currentDataset = removeNegativeAgeOfPlanes(currentDataset)
    currentDataset = transformCategorical(currentDataset)
    // check if age of plane is greater than zero
    val noValuesTotal = currentDataset.count()
    println(s"Remaining values for the merged dataframe: $noValuesTotal")
    MyApp.indexlist = createNewIndexList(currentDataset)


    currentDataset = dropCategoricalColumns(currentDataset)

    val naCountMerged = currentDataset.columns.map(c => (c, currentDataset.filter(col(c) === "NA" || col(c) === "UNKNOW" || col(c).isNull || col(c).isNaN).count()))
    println(naCountMerged.mkString("Array(", ", ", ")"))

    currentDataset.show()
    currentDataset
  }

  /** Basic pre-processing and removing forbidden columns in df1995 **/
  var initialRemovement = (data: DataFrame)=>{
    var resData = data
    resData = resData.filter(col("Cancelled") === 0 && col("Diverted") === 0)
    resData = resData.drop("ArrTime", "ActualElapsedTime", "AirTime", "TaxiIn", "Diverted","Cancelled", "CarrierDelay", "WeatherDelay", "NASDelay", "SecurityDelay", "LateAircraftDelay")
    resData
  }

  var checkEmpty = (data: DataFrame) =>{
    var resData = data
    if (resData.isEmpty) {
      print("WARNING: The dataset is empty")
      resData.show()
    }
    resData
  }

  var checkColumnValueDistribution = (data: DataFrame)=>{
    var resultData = data
    //filter the unknown and na columns into an array
    val naCount = resultData.columns.map(c => (c, resultData.filter(col(c) === "NA" || col(c) === "UNKNOW" || col(c).isNull).count()))

    //delete the columns where the unkow or na values percentage are higher then 90%
    for (i <- naCount.indices) {
      if ((naCount(i)._2 / data.count()) >= 0.9) {
        resultData = resultData.drop(naCount(i)._1)
        println("Column deleted: " + naCount(i)._1)
      }
    }
    resultData
  }

  var mergeDateColumns = (data: DataFrame)=> {
    var resultData = data
    //Merge 3 date columns into 1 variable

    resultData = resultData.withColumn ("mergedDate", concat (col ("Year"), lit ("-"), col ("Month"), lit ("-"), col ("DayOfMonth") ) )
    resultData
  }

  var castDataType = (data: DataFrame)=>{
    var resultData = data
    //cast the previously filled with NA and UNKNOW valued columns to int
    resultData = resultData.withColumn("DepTime", col("DepTime").cast(IntegerType))
    resultData = resultData.withColumn("CRSElapsedTime", col("CRSElapsedTime").cast(IntegerType))
    resultData = resultData.withColumn("ArrDelay", col("ArrDelay").cast(IntegerType))
    resultData = resultData.withColumn("TaxiOut", col("TaxiOut").cast(IntegerType))
    resultData = resultData.withColumn("DepDelay", col("DepDelay").cast(IntegerType))

    // cast Distance with type String to int
    resultData = resultData.withColumn("Distance", col("Distance").cast(IntegerType))

    println(resultData.dtypes.mkString("Array(", ", ", ")"))
    resultData
  }

  var mergeDataSets = (data: DataFrame, dataPlane: DataFrame)=>{
    /** Merging the dfPlane dataset into the first one */
    /** Since it's an inner join some plane's in the original dataset won't be joined into the merged dataframe.
     * This could be either a high or a low percentage (at 2008: 3% at 1995: 63%) but since the data remains intact and usable for the others we agreed to proceed with this solution. */

    var dfMerged = data.join(dataPlane, Seq("TailNum"), "inner")
    dfMerged = dfMerged.withColumn("issue_date", date_format(to_date(col(colName = "issue_date"), fmt = "MM/dd/yyyy"), format = "yyyy-MM-dd"))
    dfMerged
  }

  var calculateAge = (data: DataFrame)=>{
    var resData = data
    resData = resData.withColumn("DaysInUsage", datediff(col("mergedDate"), col("issue_date")))
    resData
  }

  var removeNegativeAgeOfPlanes = (data: DataFrame)=>{
    /** The plane-data.csv was more likely to be created for the 2008 data.
     * For our prediction we tried to simulate the plane's age comparing the manufacturing year to the current day of the flight.
     * Sometimes the plane's age had negative values since the plane was not created yet.
     * This must have been cause by the tail number's early usage for a different plane for which we don't have data in our plane-data file.
     * We decided to keep only the plane's that have positive age since the reduction costs us only 2-5% of our data and not material compared to our results. */
    var resData = data
    val noNegValues = resData.filter(resData("DaysInUsage") < 0).count()
    println(s"Negative values count in DaysInUsage: $noNegValues")
    resData = resData.filter(resData("DaysInUsage") > 0)
    resData
  }

  var transformCategorical = (data: DataFrame)=>{
    /** Transform categorical values into numerical variables */
    var resData = data
    val categoricalCols = Array("UniqueCarrier", "Dest", "Origin", "TailNum", "type", "manufacturer", "model", "status", "aircraft_type", "engine_type")
    val categoricalIndexCols = Array("UniqueCarrierIndex", "DestIndex", "OriginIndex", "TailNumIndex", "typeIndex", "manufacturerIndex", "modelIndex", "statusIndex", "aircraft_typeIndex", "engine_typeIndex")
    val indexer1995Data = new StringIndexer().setInputCols(categoricalCols).setOutputCols(categoricalIndexCols)

    resData = indexer1995Data.fit(resData).transform(resData)
    resData
  }

  var createNewIndexList = (data: DataFrame)=>{
    /**
     * For the analysis part in order to interprete any outputs related to categorical variables,
     * we are rather interested in its actual category than in its Index.
     * Therefore we create an index list.
     */
    var resData = data
    resData = resData.select("UniqueCarrier", "UniqueCarrierIndex", "Dest", "DestIndex", "Origin", "OriginIndex", "TailNum","TailNumIndex", "type","typeIndex", "manufacturer", "manufacturerIndex","model", "modelIndex","status", "statusIndex","aircraft_type", "aircraft_typeIndex","engine_type", "engine_typeIndex")
    resData
  }

  var dropCategoricalColumns = (data: DataFrame)=>{
    var resData = data
    //drop the un-necessary string and converted categorical columns first
    resData = resData.drop(colNames = "TailNum", "UniqueCarrier", "Origin", "Dest", "mergedDate", "type", "manufacturer", "model", "status", "aircraft_type", "engine_type", "issue_date", "year")
    resData
  }
  
}
