package upm.bd
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.stat.Correlation
import org.apache.spark.sql.functions.{col, column, corr, monotonically_increasing_id}
import org.apache.spark.sql.{DataFrame, DataFrameStatFunctions, Dataset, Encoder, Encoders, Row, SQLContext, SparkSession}
import org.sparkproject.dmg.pmml.False
import upm.bd.MyApp.{spark, sqlContext}
import spark.implicits._

import java.io.PrintWriter
import scala.collection.mutable


object Analysis {
  import spark.implicits._
  /**
   * basic statistic summary for every column
   */
  def summaryStatistics(data: DataFrame, path: String): Unit ={
    data.summary().write.csv(path+"/summary")
  }

  /**
   * When is the best month and best day and best hour in the week for minimizing delay?
   */
  def getHour(data: DataFrame, path: String): Unit ={
    var updatedList = data.withColumn("DepTime", (col("DepTime")/100).cast("Integer"))
    var df = updatedList.groupBy("DepTime").avg("ArrDelay").as("AverageDelay")
    df.write.csv(path++"/hour")
    df.show()

  }

  def getMonth(data: DataFrame, path: String): Unit ={
    var df = data.groupBy("Month").avg("ArrDelay").as("AverageDelay2")
    df.show()
    df.write.csv(path++"/month")
  }

  def getDay(data: DataFrame, path: String): Unit = {
    var df = data.groupBy("DayOfWeek").avg("ArrDelay").as("AverageDelay3")
    df.show()
    df.write.csv(path+"/day")

  }

  /**
   * most traveled airports
   */

  def countAirports(data: DataFrame, indexlist: DataFrame, path: String): Unit ={

    var origin = data.groupBy("OriginIndex").count()
    var index2 = indexlist.select("OriginIndex", "Origin")
    origin = origin.join(index2, Seq("OriginIndex")).distinct()
    origin.show()
    origin.write.csv(path+"/origin")

    var dest = data.groupBy("DestIndex").count()
    var index = indexlist.select("DestIndex", "Dest")
    dest = dest.join(index, Seq("DestIndex")).distinct()
    dest.show()
    dest.write.csv(path+"/dest")
  }

  /**
   * Correlation matrix
   */
  def correlationMatrix(data: DataFrame): Unit ={
    val columnLength = data.columns.length -1
    var matrix = spark.emptyDataFrame
    var indicator : Boolean = false;


    for (x <- 0 to columnLength) {

      val corrList = mutable.MutableList[Double]()

      for (y <- 0 to columnLength) {

        val colX = data.columns(x)
        val colY = data.columns(y)

        val coeff = data.stat.corr(col1 = colX, col2 = colY)

        //println(s"The correlation between the columns $colX and $colY is: ${coeff.toString}")
        corrList += coeff
      }
      corrList.toDF().withColumnRenamed("value", data.columns(x)).show()
    }
    /*
    val assembler = new VectorAssembler()
      .setInputCols(data.columns)
      .setOutputCol("vectors")

    val transformed = assembler.transform(data)

    val corr = Correlation.corr(transformed, "vectors").head

    println(s"Pearson correlation matrix:\n $corr")
    */
  }

}
