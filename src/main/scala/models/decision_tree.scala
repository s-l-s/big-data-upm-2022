package upm.bd

import org.apache.spark.ml.Pipeline
import org.apache.spark.sql.DataFrame
import org.apache.spark.ml.regression.DecisionTreeRegressionModel
import org.apache.spark.ml.regression.DecisionTreeRegressor
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}


import upm.bd.Model

class customDecisionTreeRegressionModel () extends Model() {
  val decisionTreeModel = new DecisionTreeRegressor()
       .setLabelCol("label")
       .setFeaturesCol("features")
      
  val pipeline = new Pipeline().setStages(Array(oneHotEnc, numericalVect, minMaxEnc, featureVect, decisionTreeModel))

  // Define the parameter grid
  val paramGrid = new ParamGridBuilder().addGrid(decisionTreeModel.maxDepth, Array(3, 5, 7)).addGrid(decisionTreeModel.minInfoGain, Array(0.1, 0.3, 0.5)).build()

  val cv = new CrossValidator().setEstimator(pipeline).setEvaluator(r2Evaluator).setEstimatorParamMaps(paramGrid).setNumFolds(3)

  def cvModel(trainDF: DataFrame, testDF: DataFrame): DataFrame = {
    val trainedModel = cv.fit(trainDF)
    val predictions = trainedModel.transform(testDF)
    predictions
    }
}
