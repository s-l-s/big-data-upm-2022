package upm.bd

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.sql.DataFrame
import org.apache.spark.ml.regression.{LinearRegression}
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}

import upm.bd.Model

class LinearModel () extends Model() {
  val linearModel = new LinearRegression()
       .setLabelCol("label")
       .setFeaturesCol("features")
      
  val pipeline = new Pipeline().setStages(Array(oneHotEnc, numericalVect, minMaxEnc, featureVect, linearModel))

  // Define the parameter grid
  val paramGrid = new ParamGridBuilder().addGrid(linearModel.regParam, Array(0.1, 0.01)).addGrid(linearModel.elasticNetParam, Array(0.5, 0.7)).build()

  // // Define the cross-validator
  val cv = new CrossValidator().setEstimator(pipeline).setEvaluator(r2Evaluator).setEstimatorParamMaps(paramGrid).setNumFolds(3)

  def cvModel(trainDF: DataFrame, testDF: DataFrame): DataFrame = {
    val trainedModel = cv.fit(trainDF)
    val predictions = trainedModel.transform(testDF)
    println(predictions)
    predictions
    }
}
