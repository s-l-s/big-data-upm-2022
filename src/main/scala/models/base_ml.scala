package upm.bd

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.feature.{VectorAssembler, MinMaxScaler, OneHotEncoder}
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

class Model() {

def transformInputData(mergedDF: DataFrame): DataFrame = {
  val inputDF = mergedDF.select(col("typeIndex"), col("TaxiOut"), col("DaysInUsage"), col("UniqueCarrierIndex"), col("DestIndex"), col("OriginIndex"), col("TailNumIndex"), col("manufacturerIndex"), col("modelIndex"), col("statusIndex"), col("aircraft_typeIndex"), col("engine_typeIndex"), col("Month"), col("DayofMonth"), col("DayofWeek"), col("DepTime"), col("CRSDepTime"), col("FlightNum"), col("DepDelay"),  col("Distance"), col("ArrDelay").cast("Double").alias("label"))

inputDF
}
       
  val oneHotEnc = new OneHotEncoder().setInputCols(Array(
  "TaxiOut", "typeIndex", "UniqueCarrierIndex", "DestIndex", "OriginIndex", "TailNumIndex", "manufacturerIndex", "modelIndex", "statusIndex", "aircraft_typeIndex", "engine_typeIndex", "Month", "DayofMonth", "DayofWeek", "FlightNum")).setOutputCols(Array("TaxiOutVec", "typeIndexVec", "UniqueCarrierIndexVec", "DestIndexVec", "OriginIndexVec", "TailNumIndexVec", "manufacturerIndexVec", "modelIndexVec", "statusIndexVec", "aircraft_typeIndexVec", "engine_typeIndexVec", "MonthVec", "DayofMonthVec", "DayofWeekVec", "FlightNumVec")).setHandleInvalid("keep")

   val numericalVect = new VectorAssembler().setInputCols(Array("DaysInUsage", "DepDelay", "Distance", "DepTime", "CRSDepTime")).setOutputCol("numFeatures").setHandleInvalid("skip")

   val minMaxEnc = new MinMaxScaler().setInputCol(numericalVect.getOutputCol).setOutputCol("normFeatures")

   val featureVect = new VectorAssembler().setInputCols(Array("TaxiOutVec", "typeIndexVec", "UniqueCarrierIndexVec", "DestIndexVec", "OriginIndexVec", "TailNumIndexVec", "manufacturerIndexVec", "modelIndexVec", "statusIndexVec", "aircraft_typeIndexVec", "engine_typeIndexVec", "MonthVec", "DayofMonthVec", "DayofWeekVec", "FlightNumVec", "normFeatures")).setOutputCol("features")
   
val rmseEvaluator = new RegressionEvaluator()
    .setLabelCol("label")
    .setPredictionCol("prediction")
    .setMetricName("rmse")
  
  val r2Evaluator = new RegressionEvaluator()
    .setLabelCol("label")
    .setPredictionCol("prediction")
    .setMetricName("r2")
  
  val varEvaluator = new RegressionEvaluator()
    .setLabelCol("label")
    .setPredictionCol("prediction")
    .setMetricName("var")
  


  def printEvaluationMetrics(predictions: DataFrame){
    val rmse = rmseEvaluator.evaluate(predictions)
    println(s"Root Mean Squared Error (RMSE) on test data = $rmse")

    val r2 = r2Evaluator.evaluate(predictions)
    println(s"R-squared on test data = $r2")

    val variance = varEvaluator.evaluate(predictions)
    println(s"Variance on test data = $variance")}

}
